package br.com.quarkus.kafka.producer.example.producer;

import java.util.Properties;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

@ApplicationScoped
public class Producer {
    public void execute(String topic, String material) {
        // props define propriedades para o kafka, passando a configuração do servidor
        // e configurando o tipo da chave e o tipo do valor que será enviado.
        // a chave é um uuid randomico e o valor recebe o matrial já convertido pelo mapper
        // mensagem esta sendo enviada na partição 0
        // após envio produtor é finalizado

        //Este valores de propriedades podem ser configurados no application.properties e carregado aqui
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        final String key = UUID.randomUUID().toString();
        final KafkaProducer<String, String> producer = new KafkaProducer<>(props);
        final ProducerRecord<String, String> record = new ProducerRecord<>(topic, 0, key, material);
        producer.send(record);
        producer.close();
    }
}
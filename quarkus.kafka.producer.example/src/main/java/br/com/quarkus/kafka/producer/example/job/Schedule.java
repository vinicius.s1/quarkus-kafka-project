package br.com.quarkus.kafka.producer.example.job;

import java.io.IOException;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.fasterxml.jackson.databind.ObjectMapper;


import br.com.quarkus.kafka.producer.example.model.Material;
import br.com.quarkus.kafka.producer.example.producer.Producer;
import io.quarkus.scheduler.Scheduled;


//Utilizando para a injeção de dependência
//é criado uma vez durante a duração do aplicativo.
@ApplicationScoped
public class Schedule {
    
    @Inject
    Producer producer;

    /**
     * A cada 20 segundos é realizado o envio de uma mensagem kafka
     * No tópico kafka-example-topic, este valor será utilizando posteriormente pelo consumer para realizar a leitura 
     * neste tópico em específico
     */
    @Scheduled(every = "20s")
    void runTwentySeconds() {
        Material material = new Material(UUID.randomUUID().toString(), "material de construção");
        String json = getJson(material);
        producer.execute("kafka-example-topic", json);
    }

    private String getJson(Material material) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(material);
        } catch (IOException e) {
            return null;
        }
    }
}
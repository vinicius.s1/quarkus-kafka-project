package br.com.quarkus.kafka.producer.example;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeMockResourceIT extends MockResourceTest {

    // Execute the same tests but in native mode.
}
package br.com.quarkus.kafka.consumer.example.consumer;

import javax.enterprise.context.ApplicationScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class Consumer {

    public void onReceiveMessage(String message){
        Logger loger = LoggerFactory.getLogger(Consumer.class);
        loger.info("****Menssagem recebida pelo consumer***");
        loger.info(message);
    }
    
}
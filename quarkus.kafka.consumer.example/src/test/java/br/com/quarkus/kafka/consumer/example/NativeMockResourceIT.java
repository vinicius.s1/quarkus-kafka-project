package br.com.quarkus.kafka.consumer.example;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeMockResourceIT extends MockResourceTest {

    // Execute the same tests but in native mode.
}